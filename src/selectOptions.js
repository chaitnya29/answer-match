import React from 'react';
import { Dropdown, Label, Form, List, Item, Header } from 'semantic-ui-react';
import './App.css';
import axios from 'axios';

let ListElement = (props) => {

    return (<div>
        <Label>{props.name}</Label>
        <Dropdown name={props.name} inline options={props.options} selection placeholder="select" onChange={props.getItemValue} />
    </div>
    )

}
// { "key": 1, "text": "brown", "value": "brown" }
class selectOptions extends React.Component {
    constructor() {
        super()
        this.state = {
            answers: {},
            quest: [],
            error: []
        }
        this.opt = []
        this.answers = {}
        this.getState()
    }
    componentDidMount() {
        // this.optionsA.map(data => this.answers[data] = null)
        // this.setState({ answers: this.answers })
    }
    getState() {
        axios.get('http://localhost:3006/question/1', this.state.answers)
            .then(
                (response) => {
                    console.log(response.data)
                    this.setState({
                        quest: response.data
                    }, () => {
                        this.state.quest.option.map(
                            option => option.B.map(
                                (B, index) => this.opt.push({
                                    key: index,
                                    text: B,
                                    value: B
                                })
                            )
                        )
                    })
                    console.log(this.opt)
                }
            )
            .catch(
                (error) => {
                    console.log(error)
                }
            )
    }
    render() {
        let alertme = (e) => {
            Object.keys(this.state.answers).map(
                (data) => this.state.answers[data] === null ? this.state.error.push('please select the answer for ' + data) : null)
            this.setState({
                error: this.state.error
            })
            let data = this.state.quest
            data.answers = this.state.answers
            this.setState({
                quest: data
            })
            console.log(this.state.quest)
            // if (this.state.error.length === 0) {
            //     axios.put('http://localhost:3006/question/1', this.state.answers)
            //         .then((response) => {
            //             console.log(response.data)
            //         })
            //         .catch((error) => {
            //             console.log(error)
            //         })
            // }
        }

        let getItemValue = (e, data) => {
            this.answers[data.name] = data.value
            this.setState({
                answers: this.answers,
            })
        }

        let getResult=()=>{
            let trueAns = Object.keys(this.state.quest.TrueAnswers[0])
            let ans = {}
            trueAns.map(
                data => ans[data] = this.state.quest.TrueAnswers[0][data]===this.state.answers[data]
            )
            console.log(ans)
            alert(JSON.stringify(this.state.quest.TrueAnswers[0])===JSON.stringify(this.state.answers))
        }

        return (
            <div className='labletest'>
                <Form onSubmit={alertme}>
                    {
                        this.state.quest.option ?
                            <div>
                                <Header as="h3" content={this.state.quest.question} />
                                {
                                    this.state.quest.option.map(option => option.A.map(
                                    A => <ListElement name={A} options={this.opt} getItemValue={getItemValue} />
                                    ))
                                }
                            </div>
                            :
                            null
                    }
                    <button type='submit'>submit</button>
                </Form>
                    <button onClick={getResult}>get result</button>
            </div>

        )
    }
}

export default selectOptions