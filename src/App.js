import React, { Component } from 'react';
import axios from 'axios';
// import SelectOptions from './selectOptions'
import BucketList from './BucketList'



class App extends Component {
  constructor(props){
    super(props);
    this.state={
      data : ''
    }
    this.getstate()
  }
    getstate = () => {
      axios.get('http://localhost:3006/question/1')
        .then((res) => this.setState(
          {
            data: res.data
          },console.log(res.data)))
        .catch(err => console.log(err))
  }
  render() {
    return (<div>
       { this.state.data.Answer ?
        <BucketList data={this.state.data}/>
        :
        null}
      </div>
    );
  }
}

export default App;
