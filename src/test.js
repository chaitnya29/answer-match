import React from 'react'
import { Grid, Segment, Input, Button, Container, Checkbox } from 'semantic-ui-react';
import ItemContainer from './itemContainer';
import ItemCreator from './itemCreator';
import ItemsDragger from './itemsDragger';
import ItemsBucket from './itemsBucket'
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';

class AddBucketQT extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      item: '',
      items: [],
      bucketsItem: '',
      buckets: [],
      data: {
        Answer: [
          {
            "Items": [],
            "Buckets": []

          }
        ]
      }
    };

  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  addItem = () => {
    let items = this.state.items
    items.push(this.state.item)
    const ans = {
      Answer: [{
        Items: items,
        Buckets: this.state.data.Answer[0]["Buckets"]
      }]
    }
    this.setState({
      item: '',
      data: ans
    })
  }
  addBucket = () => {
    let buckets = this.state.buckets
    buckets.push(this.state.bucketsItem)
    const ans = {
      Answer: [{
        Items: this.state.data.Answer[0]["Items"],
        Buckets: buckets
      }]
    }
    this.setState({
      bucketsItem: '',
      data: ans
    })
  }
  deleteItem = (e) => {
    let items = this.state.items
    items.splice(e.target.id, 1);
    const ans = {
      Answer: [{
        Items: items,
        Buckets: this.state.data.Answer[0]["Buckets"]
      }]
    }
    this.setState({
      items,
      viewDemo: false,
      data: ans,
    })
  }
  deleteBucket = (e) => {
    let buckets = this.state.buckets
    buckets.splice(e.target.id, 1);
    const ans = {
      Answer: [{
        Items: this.state.data.Answer[0]["Items"],
        Buckets: buckets
      }]
    }
    this.setState({
      buckets,
      viewDemo: false,
      data: ans
    })
  }
  
  submitBucket = () => {
    const items = this.state.items;
    const buckets = this.state.buckets;
    let itemArr = [];
    let bucketArr = []
    items.map((data, index) => itemArr.push({
      ItemID: index,
      Value: data,
      'Content-type': 'text'
    }))
    buckets.map((data, index) => bucketArr.push({
      ItemID: index,
      Value: data,
      'Content-type': 'text'
    }))

    console.log(this.state)

  }
  render() {
    const items = this.state.items;
    const buckets = this.state.buckets;
    const view = items.length && buckets.length > 0 ? true : false
    return (
      <Container>
        <Grid columns={2} padded>
          <Grid.Column>
            <ItemCreator name='item' addItem={this.addItem} value={this.state.item} handleChange={this.handleChange} placeholder='enter item' />
            <ItemCreator name='bucketsItem' addItem={this.addBucket} value={this.state.bucketsItem} handleChange={this.handleChange} placeholder='enter bucket name' />
            {items.length || buckets.length > 0 ?
              <Segment>
                <Input name='question' fluid placeholder='Enter your question here' value={this.state.question} onChange={this.handleChange} />
                <ItemContainer items={items} deleteItem={this.deleteItem} name='Items' />
                <ItemContainer items={buckets} deleteItem={this.deleteBucket} name='Buckets' />
                {view ?
                    <Checkbox toggle as='b' label='craete true answer' checked={this.state.viewDemo} onChange={(e, data) => this.setState({ viewDemo: data.checked })} />
                  :
                  null
                }
              </Segment>
              :
              null
            }
            {view ? <Button content='submit' onClick={this.submitBucket} /> : null}

          </Grid.Column>
          <Grid.Column>
            <ItemsDragger items={this.state.items} view={this.state.viewDemo} />
            <ItemsBucket buckets={this.state.buckets} view={this.state.viewDemo} />
          </Grid.Column>

        </Grid>

      </Container>
    )
  }
}


export default  DragDropContext(HTML5Backend)(AddBucketQT)