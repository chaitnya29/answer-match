import React from 'react'
import { Input, Button } from 'semantic-ui-react';

let itemCreator = ({name,addItem,handleChange,value,placeholder})=>{
    return(
        <div>
        <Input name={name} size='tiny' placeholder={placeholder}

        value={value} onChange={handleChange} />
        <Button name={name} size='tiny' onClick={addItem}>add</Button>
        </div>
    )
}

export default itemCreator