import React from 'react'
import { Grid } from 'semantic-ui-react';

class ItemsBucket extends React.Component {
    constructor() {
        super()
        this.state = {
            buckets: [],
            view : false
        }
        this.dropBucket = this.dropBucket.bind(this)
        this.Answer={}
    }
    componentWillReceiveProps(n) {
            this.setState({
                view : n.view,
                buckets: n.buckets,
               
            })
    }
    allowDrop = (ev) => {
        ev.preventDefault();
    }
    dropBucket(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("name");
        console.log(ev.dataTransfer.getData("from"))
        console.log(ev.target.id, data);
        ev.target.appendChild(document.getElementById(data));
        this.setState({ data })
        this.Answer[ev.target.id] !== undefined ? this.Answer[ev.target.id].push(data) : this.Answer[ev.target.id] = [data]
        // let i = this.Answer[parentId].indexOf(data);
        // console.log(this.Answer[parentId]);
        // this.Answer[parentId].splice(i, 1);
        console.log('ans ', this.Answer)
        // this.props.respnder(this.Answer)
    }
    render() {
        return (
            this.state.view?
            <Grid>
                {
                    this.state.buckets.map((data, index) => {
                        return (
                            <Grid.Column key={index} width={8}>
                                <h3>{data}</h3>
                                <div className='category' id={data} onDrop={this.dropBucket} onDragOver={this.allowDrop}> </div>
                            </Grid.Column>

                        )
                    })
                }
            </Grid>
            :
            null
        )
    }
}

export default ItemsBucket