import React from 'react';
import './App.css';
import { Dropdown, Label, Form, List, Item, Header } from 'semantic-ui-react';
import './App.css';
import axios from 'axios';

let BoxItem = (props) => {
    return (<div>
        <Label>{props.name}</Label>
        <Dropdown name={props.name} inline options={props.options} selection placeholder="select" onChange={props.getItemValue} />
    </div>
    )

}
class Box extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            quest: [
                1, 2, 3, 4
            ],
            swapId: [0, 0]
        }
        this.answers = {}
    }


    // render() {
    //     return (
    //         Array(5).fill().map((d, i) => <div> {Array(2).fill(i).map(d => <div class="box">
    //             <h1>{d}</h1>
    //         </div>)}</div>
    //         )
    //     )
    // }



    onDragOver_handler = (ev) => {
        // alert("in drop");
        ev.preventDefault();
        ev.stopPropagation();
        let ids = this.state.swapId;
        ids[1] = ev.target.id;
        console.log(ev.target.id);
        this.setState({
            swapId: ids
        })
        if (parseInt(this.state.swapId[0]) < parseInt(this.state.swapId[1])) {
            let startIndex = parseInt(this.state.swapId[0])
            let endIndex = parseInt(this.state.swapId[1])
            let quest = this.state.quest
            console.log(startIndex)
            let tmp = this.state.quest[startIndex];
            for (let i = startIndex; i < endIndex; i++) {
                quest[i] = quest[i + 1]
            }
            quest[endIndex] = tmp;

            this.setState({
                quest
            })
        }
        else {
            let startIndex = parseInt(this.state.swapId[0])
            let endIndex = parseInt(this.state.swapId[1])
            let quest = this.state.quest
            console.log(endIndex)
            let tmp = this.state.quest[startIndex];
            for (let i = startIndex; i > endIndex; i--) {
                quest[i] = quest[i-1]
            }
            quest[endIndex] = tmp;

            this.setState({
                quest
            })
        }
        // let items = this.state.quest
        // let temp = items[this.state.swapId[1]]
        // items[this.state.swapId[1]] = items[this.state.swapId[0]]
        // items[this.state.swapId[0]] = temp

        // this.setState({
        //     quest: items
        // })

    }
    dragstart_handler = (ev) => {
        ev.stopPropagation();
        console.log("start")
        //console.log(ev.target.id);
        let temp = parseInt(ev.target.parentNode.id);
        let startId = this.state.swapId;
        startId[0] = temp;
        this.setState({
            swapId: startId
        })

        // ev.dataTransfer.effectAllowed = "move";
        // ev.dataTransfer.setData("text/html", ev.target.parentNode);
        // ev.dataTransfer.setDragImage(ev.target.parentNode, 20, 20);
    }
    allowDrop = (ev) => {
        ev.preventDefault();
    }

    render() {
        let myobj = {
            "width": "350px",
            "height": "70px",
            "padding": "10px",
            "border": " 1px solid #aaaaaa"
        }
        let pobj = {
            "background-color": "transperent",

        }
        return (
            this.state.quest.map((data, index) => {
                return <div key={index} id={index} onDrop={this.onDragOver_handler} onDragOver={this.allowDrop} style={myobj}>
                    <p style={pobj} draggable="true" onDragStart={this.dragstart_handler} > {data} </p>

                </div>
            })
        )
    }
}

export default Box
