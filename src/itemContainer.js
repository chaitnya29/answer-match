import React from 'react'
import { Label, Icon } from 'semantic-ui-react';

let itemContainer = ({items,deleteItem,name}) => {
    const square = { width: 100, height: 100, margin: '1px',border:'1px solid' ,display: 'table-cell' }
    return (
        <div>
            {items.length > 0 ?
                <div style={{padding:'6px'}}>
                
                    <div>{name}</div>
                    {   name === 'Buckets' ?  items.map((item, index) =>  <Label basic style={square} key={index}> {item} <Icon id={index} name='delete' onClick={deleteItem} /> </Label>  ) :
                        items.map((item, index) => <Label key={index}> {item} <Icon id={index} name='delete' onClick={deleteItem} /> </Label>)
                    }
                </div>
                :
                null}
        </div>
    )
}

export default itemContainer