import React from 'react'
import './App.css'
import { DragSource } from 'react-dnd';
class ItemDragger extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items: [],
            view:false
        }
        this.dropBucket = this.dropBucket.bind(this)
        this.Answer = {}
        this.from='';
    }
    componentWillReceiveProps(n) {
            this.setState({
                view:n.view,
                items: n.items
            })
    }
    allowDrop = (ev) => {
        ev.preventDefault();
    }
    drag = (ev) => {
        ev.dataTransfer.setData("name", ev.target.id);
        ev.dataTransfer.setData('from', ev.target.parentNode.id)
        this.from = ev.target.parentNode.id;
        console.log(this.from);
    }
    onDragOverP = (ev) => {
        ev.stopPropagation();
    }
    dropBucket(ev) {
        ev.preventDefault();
        let data = ev.dataTransfer.getData("name");
        let parentId =  ev.dataTransfer.getData("from");
        console.log(ev.target.id, data ,parentId);
        ev.target.appendChild(document.getElementById(data));
        this.Answer[ev.target.id] !== undefined ? this.Answer[ev.target.id].push(data) : this.Answer[ev.target.id] = [data]
        // let i = this.Answer[parentId].indexOf(data);
        // console.log(this.Answer[parentId], i);
        // this.Answer[parentId].splice(i, 1);
        // console.log('ans ', this.Answer)
    }
    
    render() {
        let items = this.state.items.map((data, index) => {
            // return <p key={index} className='pobj' id={data} draggable="true" onDragStart={this.drag} onDragOver={this.onDragOverP} > {data} </p>
            return <p key={index} className='pobj' id={data}> {data} </p>

        })
        return ( this.state.view?
            <div className="styl" id="Items" onDrop={this.dropBucket} onDragOver={this.allowDrop}> {items} </div>
            :
            null
        )
    }
}
const cardSource = {
    beginDrag: props => {
        console.log(props)
        return { text: props.text }
    }
  }

  function collect(connect, monitor) {
    return {
      dragSource: connect.dragSource(),
      isDragging: monitor.isDragging(),
    }
  }
export default DragSource('item',cardSource,collect)(ItemDragger)