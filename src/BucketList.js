import React from 'react'
import { Grid, Header, Segment, Button } from 'semantic-ui-react';
import Bucket from './Bucket'

class BucketList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      Answer: props.data.Answer,
      Answers: ''
    }
    this.from = ''
    this.Answer = []
    this.drop = this.drop.bind(this)
    console.log(props.data.Answer)
  }
  componentDidMount(props) {
    this.getstate()
    console.log(this.Answer)
  }
  componentWillReceiveProps(nextProps){
   this.setState({
     Answer : nextProps.data.Answer
   })
  }
  getstate = () => {
    this.Answer = Object.assign({}, this.state.Answer[0]);
    this.Answer.Buckets.map(data => this.Answer[data] = [])

  }
  allowDrop = (ev) => {
    ev.preventDefault();
  }

  

  drag = (ev) => {
    ev.dataTransfer.setData("name", ev.target.id);

    console.log(ev.target.parentNode.id);
    this.from = ev.target.parentNode.id;
    console.log(this.from);


  }
  onDragOverP = (ev) => {
    ev.stopPropagation();
    //console.log(ev.target);
  }
  drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("name");
    console.log(ev.target.id, data);
    try {
      ev.target.appendChild(document.getElementById(data));
      this.Answer[ev.target.id] !== undefined ? this.Answer[ev.target.id].push(data) : this.Answer[ev.target.id] = [data]
    } catch (error) {
      console.log(error)
    }
    let i = this.Answer[this.from].indexOf(data);
    console.log(this.Answer[this.from], i);
    this.Answer[this.from].splice(i, 1);
    console.log('ans ', this.Answer)
    //console.log('true ans', this.state.TrueAnswer[0]);
  }
  

  render() {
    console.log(this.state)
    let styl = {
      "minWidth": "350px",
      "minHeight": "120px",
      "minPadding": "10px",
      "border": "1px solid #aaaaaa"

    }
    let category = {
      "minWidth": "70px",
      "minHeight": "120px",
      "minPadding": "10px",
      "border": "1px solid #aaaaaa"
    }

    let pobj = {
      "backgroundColor": "aquamarine",
      "display": "inline-flex",
      "padding": "inherit",
      "margin": "10px"
    }
    
    let items = this.state.Answer[0].Items.map((data, index) => {
      return <p key={index} style={pobj} id={data} draggable="true" onDragStart={this.drag} onDragOver={this.onDragOverP} > {data} </p>
    })
    
    return (
    <div>
        <Header as="h3">{this.state.quesion}</Header>
        <h3>Items</h3>
        <div className="items" id="Items" style={styl} onDrop={this.drop} onDragOver={this.allowDrop}> {items} </div>
        <Grid>
          {
            <Bucket data={this.state.Answer[0].Buckets} style={category} onDrop={this.drop} onDragOver={this.allowDrop} />
          }
        </Grid>
      </div >
    )
  }
}
export default BucketList