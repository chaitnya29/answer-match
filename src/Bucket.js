import React from 'react'
import { Grid } from 'semantic-ui-react';


let bucket = (props)=>{
    return(
        props.data.map((data, index) => {
            return (<Grid.Column key={index} width={8}>
              <h3>{data}</h3>
              <div id={data} style={props.style} onDrop={props.onDrop} onDragOver={props.onDragOver}> </div>
            </Grid.Column>)
          })
    )
}

export default bucket