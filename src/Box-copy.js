import React from 'react';
import { Segment } from 'semantic-ui-react';

class Box extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            quest: [0, 1, 2, 3],

            swapId: [0, 0]
        }
        this.answers = {}
    }


    onDragOver_handler = (ev) => {
        // alert("in drop");
        ev.preventDefault();
        ev.stopPropagation();
        let ids = this.state.swapId;
        ids[1] = ev.target.id;
        console.log(ev.target.id);
        this.setState({
            swapId: ids
        })
        if (parseInt(this.state.swapId[0]) < parseInt(this.state.swapId[1])) {
            let startIndex = parseInt(this.state.swapId[0])
            let endIndex = parseInt(this.state.swapId[1])
            let quest = this.state.quest
            console.log(startIndex)
            let tmp = this.state.quest[startIndex];
            for (let i = startIndex; i < endIndex; i++) {
                quest[i] = quest[i + 1]
            }
            quest[endIndex] = tmp;
            this.setState({
                quest
            })
            // this.props.responder(this.state.quest)
        }
        else {
            let startIndex = parseInt(this.state.swapId[0])
            let endIndex = parseInt(this.state.swapId[1])
            let quest = this.state.quest
            console.log(endIndex)
            let tmp = this.state.quest[startIndex];
            for (let i = startIndex; i > endIndex; i--) {
                quest[i] = quest[i - 1]
            }
            quest[endIndex] = tmp;

            this.setState({
                quest
            })
            // this.props.responder(this.state.quest)
        }
        // let items = this.state.quest
        // let temp = items[this.state.swapId[1]]
        // items[this.state.swapId[1]] = items[this.state.swapId[0]]
        // items[this.state.swapId[0]] = temp

        // this.setState({
        //     quest: items
        // })

    }
    dragstart_handler = (ev) => {
        ev.stopPropagation();
        console.log("start")
        //console.log(ev.target.id);
        let temp = parseInt(ev.target.parentNode.id);
        let startId = this.state.swapId;
        startId[0] = temp;
        this.setState({
            swapId: startId
        })

        // ev.dataTransfer.effectAllowed = "move";
        // ev.dataTransfer.setData("text/html", ev.target.parentNode);
        // ev.dataTransfer.setDragImage(ev.target.parentNode, 20, 20);
    }
    allowDrop = (ev) => {
        console.log(ev);
        ev.preventDefault();
        // ev.target.style.border = "4px dotted green";

        //ev.target.attr.class="ondragoverclass";
    }
    onmouseoutevent = (ev) => {
        let myobj = {
            // "width": "350px",
            //  "height": "70px",
            //  "padding": "10px",
            // "border-bottom": " 1px solid #aaaaaa"
            "border": "4px dotted green"
        }
        ev.preventDefault();
        ev.target.style = myobj;
    }

    render() {
        let myobj = {
            // "width": "350px",
            "height": "70px",
            "padding": "10px",
            "border-bottom": " 1px solid #aaaaaa"
        }
        let pobj = {
            "background-color": "gainsboro",
            "border": " 1px solid #aaaaaa",
            "display": "inline-block",
            "padding": "5px"


        }
        let ondragoverclass = {
            "border": " 1px solid yellow"
        }
        return (
            this.state.quest.map((data, index) => {
                return <div><Segment> <div key={index} id={index} onDrop={this.onDragOver_handler} onDragOver={this.allowDrop} style={myobj}> </div></Segment>
                    <p style={pobj} draggable="true" onDragStart={this.dragstart_handler} onDragOver={this.allowDrop} > {data} </p>
                </div>
            })
        )
    }
}

export default Box
