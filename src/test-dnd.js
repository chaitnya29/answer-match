import React from 'react'
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    console.log(result)
    return result;
};
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items: [
                { id: "item-0", content: "item 0" },
                { id: "item-1", content: "item 1" },
                { id: "item-2", content: "item 2" },
                { id: "item-3", content: "item 3" }
            ]
        }
        console.log(this.state)
    }
    onDragStart = (r) => {
        console.log(r)
    };
    onDragEnd = (result) => {
        if (!result.destination) {
            return;
        }

        const items = reorder(
            this.state.items,
            result.source.index,
            result.destination.index
        );

        this.setState({
            items
        });
    };

    render() {

        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                <Droppable droppableId='d1'>
                    {provided => (
                        <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        >
                        {this.state.items.map((data,index) => <Draggable draggableId={data} index={index}>
                            {provided=>(<div
                             ref={provided.innerRef}
                             {...provided.draggableProps}
                             {...provided.dragHandleProps}
                             key={data} style={{ 'margin': '5px', 'border': 'black solid', 'padding': '5px', 'width': '100px' }}>{data.content}</div>)}
                        </Draggable>)}
                        {provided.placeholder}
                        </div>
                    )}
                </Droppable>

            </DragDropContext>

        )
    }
}
export default App

                      