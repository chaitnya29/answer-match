import React from 'react';
import './App.css';
import { Dropdown, Label, Form, List, Item, Header } from 'semantic-ui-react';
import './App.css';
import axios from 'axios';

let BoxItem = (props) => {
    return (<div>
        <Label>{props.name}</Label>
        <Dropdown name={props.name} inline options={props.options} selection placeholder="select" onChange={props.getItemValue} />
    </div>
    )

}
class Box extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            quest: [
                1,2,3,4
            ],
            swapId: [0, 0]
        }
        this.answers = {}
    }


    // render() {
    //     return (
    //         Array(5).fill().map((d, i) => <div> {Array(2).fill(i).map(d => <div class="box">
    //             <h1>{d}</h1>
    //         </div>)}</div>
    //         )
    //     )
    // }
    onDragOver_handler = (ev) => {
        ev.preventDefault();
        ev.stopPropagation();
        let ids = this.state.swapId;
        ids[1] = ev.target.id;
        this.setState({
            swapId: ids
        })
        let items = this.state.quest
        let temp = items[this.state.swapId[1]]
        items[this.state.swapId[1]] = items[this.state.swapId[0]]
        items[this.state.swapId[0]] = temp

        this.setState({
            quest: items
        })

    }
    dragstart_handler = (ev) => {
        ev.stopPropagation();
        console.log(ev.target.id);
        let startId = this.state.swapId;
        startId[0] = ev.target.id;
        this.setState({
            swapId: startId
        })

        // ev.dataTransfer.effectAllowed = "move";
        // ev.dataTransfer.setData("text/html", ev.target.parentNode);
        // ev.dataTransfer.setDragImage(ev.target.parentNode, 20, 20);
    }
    render() {
        let myobj={
            "background-color" : "#666",
            "border " : "1px",
            "margin" :"10px"
        }
        return (
            this.state.quest.map((data, index) => {
                return <div  key={index} id={index} onDragOver={this.onDragOver_handler} draggable="true" onDragStart={this.dragstart_handler} style={myobj}>
                    {data}
                </div>
            })
        )
    }
}

export default Box
