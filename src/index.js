import React from 'react';
import ReactDOM from 'react-dom';
import App from './demo';
import App1 from './test-dnd';
import App3 from './test';

import * as serviceWorker from './serviceWorker';
import { Grid } from 'semantic-ui-react';

let Main = ({left,right}) => (
    <Grid>
        <Grid.Row>
            <Grid.Column width={8}>
                {left}
            </Grid.Column>
            <Grid.Column width={8}>
                {right}
            </Grid.Column>
        </Grid.Row>
    </Grid>
)

ReactDOM.render(<Main left={<App3 />} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
